public class ArrayDeletionsLab2_1 {

    public static void main(String[] args) {

       int[] arr = {1, 2, 3, 4, 5};
       int[] arrDeleteIndex = new int [arr.length-1];
       int[] arrDeleteValue = new int [arrDeleteIndex.length-1];

       int index = 2; 
       int value = 4; 


        for(int i = 0; i < index ; i++){
            arrDeleteIndex[i] = arr[i];
        }

        for(int i  = index + 1; i < arr.length ; i++){
            arrDeleteIndex[i-1] = arr[i];
        }

        for(int i = 0;i<arrDeleteValue.length;i++){
            if(arrDeleteIndex[i] == value){
                arrDeleteValue[i] = arrDeleteIndex[i+1];
            }else{
                arrDeleteValue[i] = arrDeleteIndex[i];
            }
        }

        System.out.print("Original Array:");
        printArray(arr);
        System.out.print("Array after deleting element at index 2:");
        printArray(arrDeleteIndex);
        printArray(arrDeleteValue);
    }

    public static void printArray(int[] arr) {
        for(int i = 0 ; i < arr.length ; i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }

    
}