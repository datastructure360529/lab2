public class ArrayDeletionsLab2_2 {
    public static int removeElement(int[] nums, int val) {
        int k = 0; // Initialize a variable to keep track of the non-val elements
        
        for (int num : nums) {
            if (num != val) {
                nums[k] = num;
                k++;
            }
        }
        
        return k;
    }

    public static void main(String[] args) {
        int[] nums = {0,1,2,2,3,0,4,2};
        int val = 2;

        int result = removeElement(nums, val);

        System.out.print("Modified Array: ");
        for (int i = 0; i < result; i++) {
            System.out.print(nums[i] + " ");
        }
        for (int i = result; i < nums.length; i++) {
            System.out.print(" _ ");
        }

        System.out.println();
        
        System.out.println("Number of Elements: " + result);
    }
}
